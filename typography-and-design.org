#+TITLE: Typography and Design


| <2017-01-30 Mon> | http://www.64notes.com/design/stop-helvetica-arial/                                               | Typography notes                                                 |
| <2018-02-09 Fri> | https://www.reddit.com/r/programming/comments/7wc7t3/computer_color_is_broken/                    | About log colour encoding (the video is slightly inaccurate) [5] |
| <2017-01-26 Thu> | https://medium.com/@erikdkennedy/color-in-ui-design-a-practical-framework-e18cacd97f9e#.p0xqzwsxv | Good article about use of colour in UI design [2]                |
| <2017-04-12 Wed> | https://material.io/color/#!/?view.left=0&view.right=0&primary.color=00ACC1                       | Color choosing UI for material design (see also [2] above)       |
| <2018-02-09 Fri> | http://blog.johnnovak.net/2016/09/21/what-every-coder-should-know-about-gamma/                    |                                                                  |
| <2017-03-23 Thu> | https://gridstylesheets.org                                                                       | Constraint based layout for web pages                            |

* Thoughts about design and HTML

Here is a CSS Reset which would be useful to use in documents. I
should put it somewhere...
#+BEGIN_SRC css
  /* Don't forget to set a foreground and background color 
     on the 'html' or 'body' element! */
  html, body, div, span,
  applet, object, iframe,
  h1, h2, h3, h4, h5, h6, p, blockquote, pre,
  a, abbr, acronym, address, big, cite, code,
  del, dfn, em, font, img, ins, kbd, q, s, samp,
  small, strike, strong, sub, sup, tt, var,
  dd, dl, dt, li, ol, ul,
  fieldset, form, label, legend,
  table, caption, tbody, tfoot, thead, tr, th, td {
          margin: 0;
          padding: 0;
          border: 0;
          font-weight: inherit;
          font-style: inherit;
          font-size: 100%;
          line-height: 1;
          font-family: inherit;
          text-align: left;
          vertical-align: baseline;
  }
  a img, :link img, :visited img {
          border: 0;
  }
  table {
          border-collapse: collapse;
          border-spacing: 0;
  }
  ol, ul {
          list-style: none;
  }
  q:before, q:after,
  blockquote:before, blockquote:after {
          content: "";
  }

#+END_SRC

Then I can control exactly what I want to use for styling. What I
would quite like to do is change my UA stylesheet, although that might
not be a good idea, since it would make things look fine but only on
my machine. 

(The above is from
http://meyerweb.com/eric/thoughts/2007/04/14/reworked-reset/)

Here is webkit's default stylesheet:-
http://trac.webkit.org/browser/trunk/Source/WebCore/css/html.css

https://news.ycombinator.com/item?id=14548085
- basic principles of visual design. Possibly useful discussion?
- [[http://grid.malven.co/][CSS Grid guide]]
- [[https://www.gov.uk/service-manual/service-standard][UK Government Digital Service Standard]] looks pretty useful
- [[https://www.nhs.uk/news/older-people/puzzle-solving-doesnt-slow-down-mental-decline-older-people/][NHS web pages]] are pretty cleanly designed too

* Web Typography
  The Elements of Typographic Style Applied to the Web (2005):-
- https://github.com/clagnut/webtypography
- http://webtypography.net/toc/
- http://webtypography.net/
- https://news.ycombinator.com/item?id=16911850
- [[https://news.ycombinator.com/item?id=18228301][Guide to Rhythm in Web Typography]] - [[https://betterwebtype.com/rhythm-in-web-typography][article linked from HN]]

* Misc
- [[https://coolbackgrounds.io/][Cool Backgrounds]]
- https://www.gov.uk/service-manual/design
- https://github.com/cssanimation/css-animation-101
- https://github.com/LisaDziuba/Awesome-Design-Tools
- https://ishadeed.com/article/unusual-use-cases-pseudo-elements/
* Videography
Interesting [[https://www.dpreview.com/articles/0326094138/a-photographer-s-look-into-the-world-of-video/2][article]] about videography from a still shooter's
perspective. And [[https://www.dpreview.com/videos/9745357442/dpreview-tv-how-to-get-correct-exposure-when-shooting-video][here too]] which I should watch, which is where I found
it.
* Fonts
https://fonts.google.com/specimen/Open+Sans
* Photography
[[https://news.ycombinator.com/item?id=17714814][Photography Composition]] (links to [[https://antongorlin.com/blog/photography-composition-definitive-guide/][here]])

* Useful Links
- [[http://augmented-ui.com/][cyberpunk web design]] via https://news.ycombinator.com/item?id=20923792
- https://www.reddit.com/r/BeAmazed/comments/ad68pu/marcello_barenghi_hyperrealism_artist_ama/

- https://news.ycombinator.com/item?id=22324298 - HN thread about brown, link to video and various other interesting bits
