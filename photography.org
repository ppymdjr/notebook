#+TITLE: Photography

- [[https://www.reddit.com/r/Damnthatsinteresting/comments/87lqye/costume_and_lighting_changes/][Costume and Lighting Changes]]

* Lenses
It would be good to aquire another lens for wedding etc photography. At
the very least I must do more testing with the Tamron lens to see how
it performs and get nice sharp shots from it (at what aperture?). 

A 70-200 would be good for this (eg Tamron). I'd like a fast 85 or
135, but 70-200 is probably more useful for weddings.

The 100mm Pentax macro is supposed to be very very good (sharp etc),
and is weather sealed and fairly reasonable in price. The new 50mm FA*
is apparently great, but duplicates what I already have. There should
be an 85mm FA* (f1.4) out in 2019, but it won't be cheap. Might well
be a fantastic lens though. The old one is supposed to be great but
might have CA issues in some circumstances. 77mm FA limited is also
really good, but £836 (similar to the old 85mm). It's f1.7, so pretty
good and significantly more than the f2.8 of the 100mm when
needed. Pentax 70-200 f2.8 is very spendy, but sounds great. 

* General
How to take good photos in a variety of different lighting
environments making best use of the camera's capability? As a K1 is
largely ISO invariant, there is, arguably, little point in using ISO
3200 - the same result is achieved by underexposing at ISO 100 and
then pushing a lot in post (apart from time spent doing developing).

This would suggest that, if we take ISO 3200 as the highest we would
like to go in effect, which is 5 stops above ISO 100, that means I
could shoot at ISO 100 all the time *provided* I make sure not to
under expose by more than 5 stops, and provided I don't blow any
highlights. This suggests that I could set the camera to manual, ISO
100 and a sensible aperture for required DOF and then try to get the
best shutter speed I can within certain limits.

Now, I wouldn't want to underexpose if not needed, so aperture
priority and exposure comp won't really work. I wouldn't want to use
unnecessarily slow shutter speed either. This suggests I would
probably just have to go manual and watch my shutter speed *and*
degree of under/over exposure.
* Astronomy/Astrophotography
One Amazon suggestion for a present was a telescope. Is this something
J would be interested in? I don't know much about them. What kind
of price/performance ratios do they have? How do they compare to a
good telephoto lens? Rokinon 135 f/2 is recommended for astrophotography.

http://www.astropix.com/html/i_astrop/lenses.html
- that link looks pretty informative. 

The following should work well for astrophotography (especially with my camera - even w/o a tracking mount).
https://www.amazon.co.uk/Samyang-Telephoto-Pentax-Digital-Cameras/dp/B00T48C98K/ref=sr_1_2?ie=UTF8&qid=1542194598&sr=8-2&keywords=samyang+135mm+pentax

I guess part of the question would be: does a relatively inexpensive
telescope give us anything better than just sticking a half decent
lens (135mm Samyang, say) on a DSLR (which I already have) in terms of
imaging ability. If not, it might even make more sense to let J
use my old DSLR for astronomy rather than buy something inferior. I'm not sure though. 

If I did get a telescope it would be good if it *could* be used with a
DSLR - it would seem silly not to be able to take good photographs of
things when I have that capability. I would like to find time to do
some astrophotography...

Starlink tracker in comment here: https://news.ycombinator.com/item?id=22184370 (https://james.darpinian.com/satellites/?special=starlink).

Good picture of the moon here with links to info about telescopes and
astrophotography:-
https://www.reddit.com/r/pics/comments/ihaqsl/i_captured_an_85_megapixel_photo_of_our_moon_last/

* RAW Power
... by gentlement coders. Now at version 2.0. Could be a viable
replacement for Aperture - I wonder if it can do culling yet. It has a
browser. Unfortunately I suspect that using it as a Photos plugin
still inovlves working from TIFFs (though probably not on iOS).

Does that mean I can't really use the Photos library? In the manual
for the iOS version it does say it round trips and everything is non
destructive. Maybe it works... I'll have to try it.

Update as at 16/4/2020 - it is now at version 3. It's evolving to do
more of what Aperture did. It can use the Photos library directly from
the App (as PhotoScope used to). It now has star ratings (yay!) and
can also operate on folders of RAW images storing adjustments (and
previews?) in its own sidecar files in its sandbox, which I can't yet
find. It looks to be quite a bit more functional. 

It still doesn't seem that fast - I think Aperture was faster. I can't
help but wonder if I couldn't somehow make something faster in CL if I
was (?very?) cunning in terms of how I computed and cached
things. Partly I would do this by always:-
1. Not loading more data than I can show at a given time before
   starting to show it
2. Always working hard to show /something/ ASAP even if it's a crude
   approximation.

PhotoScope did the above sort of things to great success and made
browsing large libraries of RAW photos over WiFi quite a lot faster
than just sitting at the computer even on an old iPad. So it can be
done. Of course, it had previews to work from, so I may need to make
previews.

Ultimately, if I can make it good, I would really like to have
something I wrote myself so as to have control and not risk losing
it. It will take a great amount of work though. The hope would be to
leverage CLIM and CL goodness to great effect.

* Links
- https://www.bbc.com/news/in-pictures-47118130 - winner of gardgen photography awards
- https://www.bbc.com/news/in-pictures-47130038 - sony world photography awards
- https://www.reddit.com/r/Damnthatsinteresting/comments/d16ekz/cool_photography_tricks_for_your_next_adventure/
- https://www.flickr.com/groups/fuji-x-pro1/discuss/72157631913173508/ - about RAW processing
- https://www.flickr.com/groups/gmic/discuss/72157623934259058/ - more Gmic
- http://www.ricoh-imaging.co.jp/english/photo-life/astro/
- https://www.dpreview.com/news/7294676499/video-10-in-camera-tricks-for-capturing-unique-images-without-photoshop
- https://www.youtube.com/user/theartofphotography/videos - this person had some DaVinci Resolve photo tutorials apparently
- http://www.guillermoluijk.com/tutorial/dcraw/index_en.htm - very helpful guide to dcraw
- https://www.dpreview.com/opinion/3459940633/the-importance-of-emotion-in-landscape-photography
- https://www.dpreview.com/articles/5426898916/ins-and-outs-of-iso-where-iso-gets-complex
- https://news.ycombinator.com/item?id=23267827 - JS colour conversion library
- https://www.dpreview.com/articles/9010501316/fun-photo-projects-for-life-under-lockdown
- https://phillipreeve.net/blog/what-makes-a-picture-good/
* Software
Aperture won't work on Mac OS X after Mojave. I guess I need an alternative.
- Luminar
- https://affinity.serif.com/en-gb/photo/
- https://www.dxo.com

- https://github.com/maxvoltar/photo-stream

* G'Mic links
https://www.google.com/search?q=where+have+the+g%27mic+tutorials+gone&oq=where+have+the+g%27mic+tutorials+gone&aqs=chrome..69i57.7656j1j7&sourceid=chrome&ie=UTF-8
https://www.flickr.com/groups/1316653@N22/discuss/72157625021897552/
https://www.flickr.com/groups/gmic/discuss/72157623934259058/
https://www.pentaxforums.com/forums/32-digital-processing-software-printing/71060-linux-batch-raw-conversion-script-w-iso-dependent-processing.html
https://gmic.eu/tutorial/gmic-color-mapping.shtml
https://www.reddit.com/r/gmic/

* Davinci Resolve
... can be used for RAW phot editing. It might seem strange, but maybe
it's worth looking at. I like the node based workflow with the ability
to make qualifiers and power windows to do localized non destructive
edits. It's probably non ideal in some ways.

Unfortunately the free version won't deal with high resolution (4k or
larger) video, so I won't be able to export reasonable sized images
from it unless I pay for it.

This guy seems to know some things: https://www.youtube.com/watch?v=8j6ZI7X3_gg&t=331s

The Art of Photography youtube channel looks worth checking out and
has a Resolve tutorial too:
https://www.youtube.com/watch?v=CaKhcCgazP0&t=115s

** Howto
https://www.youtube.com/watch?v=Ae4xvi47IRs - video about RAW workflow. 

1. new project
2. project settings (cog)
   1. Colour science: DaVinci YRGB (color managed)
   2. Color management tab: input color space: sRGB, likewise for timeline and output
   3. Editing tab: standard still duration: 1 frame

Will that work with RAWs? The raw workflow video above is exporting an
sRGB TIFF from Capture One. Is Resolve going to be better for grading
than Capture One?

https://fstoppers.com/post-production/step-your-photography-color-editing-game-davinci-resolve-175661
* dcraw
It seems that if I use ~dcraw -v -w -q 3 -6 -T -o 4 -h IMGP4222.DNG~
it deals with exposure anyway - it spreads the histogram out over the
available image range. Underexposed RAWs look ok. If they have a lot
of DR it would be necessary to adjust the highlights somewhat.

I kind of wonder if some well chosen dcraw options would take us most
of the way there to getting nice final images. I'd still like some
more flexibility though. Need to fiddle the saturation and curves a bit. 

I should also try and find the gmic tutorials saying how to get the
flat images...

What does dcraw do if there is nothing saturated in the image? I must
find some images like that to see.
* Common Lisp

Maybe the easiest thing for me to do is to just write my own image
processing stuff in CL. How hard can it be? Applying a curve should be
pretty easy. Likewise separate curves. hue/luma curves should be
pretty easy. I would need routines to do things like colorspace
conversion. The idea would be: rather than take the input image and
separately apply a series of different transformations to it, compile
all the transformations together into one transforming function,
declare it inline and then map it over the array of pixels. I have the
feeling that things like G'Mic do the former. I would have to
experiment to find out how to get it fast.

I would probably decode the RAW files with dcraw (which does this
fine), save as 16 bit TIFF and then load that into CL. The image data
will be 108MB for 24 bit images. 216MB for 16 bit. Do we need more
than that? Then I could look into how to use MMX or whatever. Ideally
3D graphics hardware.

- https://github.com/CodyReichert/awesome-cl#graphics
- https://github.com/ruricolist/lisp-magick-wand
- https://github.com/slyrus/opticl - especially this. Might be worth looking at as starting point
- https://github.com/mfiano/pngload-fast (which is used by opticl)

It seems most likely I would have to use SBCL for this kind of
thing. Might be worth testing loop unrolling etc. I could define
functions to change pixel values in various ways then declare them
inline. It should be overall very efficient I think. We won't need to
cons at all while doing the manipulations.

Resolution of K1 RAW files: 7350 x 4912 36,103,200 pixels.
What is good for image processing? Float colourspace? 16 or 32 bit integer?

if I used 8 bits per channel with 3 channels that's 24 bits - 32 including alpha
if I use 16 bits (which is what dcraw would export) then that's 64 bits per pixel, which is a machine word

SO, how long does it take to do, say, 36 million floating point multiplications?

(see -auto-level: https://imagemagick.org/script/command-line-options.php)

** Useful links for image manipulations
- https://stackoverflow.com/questions/11163578/algorithm-to-modify-brightness-for-rgb-image
- https://en.wikipedia.org/wiki/HSL_and_HSV
- https://en.wikipedia.org/wiki/CIELAB_color_space
- http://alienryderflex.com/hsp.html - someone's modification of HSV to perceptual brightness
- https://www.cambridgeincolour.com/forums/thread47002.htm
- https://en.wikipedia.org/wiki/Gamma_correction#Microsoft_Windows,_Mac,_sRGB_and_TV/video_standard_gammas
- https://en.wikipedia.org/wiki/CIELUV
- https://en.wikipedia.org/wiki/CIE_1931_color_space#CIE_xy_chromaticity_diagram_and_the_CIE_xyY_color_space
- https://en.wikipedia.org/wiki/SRGB#The_sRGB_transfer_function_(%22gamma%22)
- https://github.com/slyrus/opticl/blob/master/gamma.lisp
- https://github.com/slyrus/opticl/tree/hsv-image
- https://psychology.wikia.org/wiki/HSL_and_HSV
- https://stackoverflow.com/questions/39118528/rgb-to-hsl-conversion
- https://stackoverflow.com/questions/8753833/exact-skin-color-hsv-range/42885200
- https://github.com/byulparan/common-cv - OpenCV bindings - looks
  like it can do [[https://www.youtube.com/watch?v=lDH7ibGp6to][real time video procesing]]. Maybe it can be used to
  display images for me as well.
- There's also cl-opencv
- http://www.guillermoluijk.com/tutorial/dcraw/index_en.htm - very good tutorial about dcraw

Is it worth using the [[https://en.wikipedia.org/wiki/CIELAB_color_space][LAB]] colorspace for doing brightness adjustments?
I should also try applying 3D LUTs. What resolution do those normally
have? How do I deal with them having relatively low resolution
compared to my sample size? Probably I need to interpolate the in
between values.

- https://www.youtube.com/watch?v=QXmfQ5sp2K0 about http://3dlutcreator.com

About LAB colour: https://graphicdesign.stackexchange.com/questions/76824/what-are-the-pros-and-cons-of-using-lab-color
Looks like it could be quite useful for doing various adjustments. 

It would be really interesting if I could, by getting this stuff
right - perhaps using LAB to more accurately model human vision for
example - I could make a program/system which would make it very easy
to produce very nice looking images. Perhaps it's more useful to apply
manipulations to the L, a and b channels than to anything else. I will
need a conversion process, which will have to assume an sRGB
colorspace I assume. Hopefully I can get images in that colorspace. I
might also risk generating colors out of sRGB gamut.

If I wanted to use LAB as the main working colorspace for various
adjustments I could also convert the whole image to that in one pass
and then apply LUTs. I guess in that sense it would be quicker for
experimenting with different adjustments (presumably). I could even
save a LAB version of the image if I wanted. It would be interesting
to try and generate a LAB image in an array and then blat it to disk
and see if I can mmap it to load it in quickly. I expect the image
file would be rather large, depending on the precision I decide to go
for.

It might be that if I want to do things to the different
representations I can cache, say, the LAB representation once I add in
things to do that. If I have a pipeline which is doing some RGB
modifications followed by some LAB modifications then I will try and
turn the RGB ones into LUTs if possible. Then I'll allow the UI to
modify the operations in the pipeline. Modifying any RGB pre-lab
operations will clear out any cached LAB image, but once we start
modifying the LAB values we'll keep the LAB image around until it
becomes invalid. That makes sense. We'll have to have a conversion to
RGB/8 at the end of course. Also, I should be able to cleverly just
operate on the number of pixels which can be shown on the screen at
once. I'm very slowly starting to work out how this will work. The
first stage could be to go from the full RGB image to a screen buffer
sized image. When working on vermeer I could make a 'quick' mode to
use non-retina pixels too. 

** Performance thoughts
I wonder how quickly I would be able to apply a very large (high
resolution) 3D LUT. Apparently they are often 33x33x33 cubes, which
doesn't seem a lot. The 1D LUTs I'm experimenting with are ~64k
entries (though they should probably be capped to just 14 bits worth).

Main memory is very slow compared to CPU speed, so one would guess
that using a LUT which exceeds cache size would have a severe penalty
due to the somewhat random access pattern into the LUT, although that
would depend on the image frequency, which often won't be that high
(more smooth gradients) so it might not be so bad. I wonder if it
would make sense to chop the 3D LUT up into sub cubes and map the
image in 4 passes ignoring RGB values not in the current pass so as to
fit the whole thing section into cache. Using a 256x256x256 cube
mapping to 8 bit values would be 16MB. Do we have 4M L2 cache? Not on
my laptop - 256KB per core. 3MB L3.

I have a baseline (eg histogram and 1D lut application) so I'll have
to try introducing different computations and see how we get on.

Liganc apparently has 8MB of smart cache, which is quite a lot. It
will be interesting to test performance of liganc for this sort of
thing. All indications (from benchmarks etc) seem to suggest that
liganc is quite powerful. It's definitely worth trying some of this
sort of stuff on liganc.

** ColorSpace
When I have dcraw output an image in some colorspace with ~-o~ it
makes a small difference to the generated image. Nothing major. The
colors look more saturated. When I read in said image into CL and then
output a 'flat' jpg I get one in the 'RGB' colorspace. It looks the
same regardless of whether the TIFF had a colorspace. I think. I need
to check that because I might be forgetting to reload the TIFF, though
I don't think so. Nope.

So, the jpg I'm outputing looks the same regardless of what TIFF I
read in. That suggests that I'm getting the same actual values in the
TIFF regardless of whether I'm applying a colorspace to it from dcraw.

I think the solution to all of this is to generate some test image
data and save it as jpeg files and then see which is right. This guide
here will help:
http://blog.johnnovak.net/2016/09/21/what-every-coder-should-know-about-gamma/

[[http://www.tawbaware.com/forum2/viewtopic.php?t=3423][This link]] would seem to suggest my TIFF files are gamma encoded, which
means I'm reading gamma data and writing gamma data, so everything is
ok. Why the images look slightly different with the some of the
profile applied I don't know. What I need, therefore, to do is to
apply the read gamma correction before processing the pixels and then
apply the reverse transformation before writing. I would like to
perform some tests by generating some gradients. 

I *think* that my image processing pipeline - eg generating per
channel LUTs to map from 16 bit input to 8 bit output - will use the
following pipeline:-
1. convert 16 bit unsigned integer to large float between 0 and 1
2. apply decode gamma conversion from sRGB
3. apply any transformation (exposure and levels etc) using a function of real->real
4. encoding gamma conversion
5. convert to unsigned 8 bit integer

For operations which are not per channel we will have to generate a
vec3 of floats for the channels. This will be needed if we want to do
things to the HSV (for example) values. 

Also, it's probably good to have dcraw fill the whole 16 bit
colorspace since it is doing whitebalance adjustment on the incoming
14 bit DAC data, so there would be some loss of resolution if we just
stuck to the 14 bits.

Colour conversions including LAB: http://www.easyrgb.com/en/math.php
* Videography
https://www.youtube.com/watch?v=_KT9mf-tarU - tips about glidecam shots. Quite interesting
https://www.youtube.com/watch?v=BfI-cpMmi30 - how to balance neewer steadicam (which is only about £41 on Amazon, so not too expensive)

I might try using my tripod as a crude steadicam and see if it helps. 
* Aperture in Catalina
- https://news.ycombinator.com/item?id=22454069
- https://medium.com/@cormiertyshawn895/deep-dive-how-does-retroactive-work-95fe0e5ea49e
* Hardware to want (if I had lots of money)
For photographing people it would be really worth having an A7 of some
kind. DPReview rate the A7R II really highly (still). Now that it's a
bit older it's more affordable [[https://www.amazon.co.uk/Sony-ILCE7RM2-Processing-Tiltable-Tru-Finder/dp/B010THXVNQ/ref=sr_1_3?dchild=1&keywords=sony+a7r+ii&qid=1596450438&sr=8-3][£1500 from Amazon]] at the moment. £169
for a 50mm f1.8 full frame lens. I wonder how good that is. 

There are some lenses I would really like. The Pentax 85mm f1.4 of
course. The 100mm Pentax macro is much cheaper and might still be
really good. 

Would be interesting to play with an A7R IV

An alternative would be to get an A6600 or A6100 which has the same
AF. A6600 has IBIS which A6100 doesn't. 

A6600 has 13.4EV vs 14.6EV for K1
A6400 is ~£900 from PC world
