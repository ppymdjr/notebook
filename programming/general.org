#+TITLE: General Programming Notes

| <2016-12-14 Wed> | https://www.youtube.com/watch?v=RplnSVTzvnU                                                   | How the blockchain will radically transform the economy - Bettina Warburg |
| <2017-03-27 Mon> | https://news.ycombinator.com/item?id=13960183                                                 | How much can your computer do in 1 second                                 |
| <2017-04-10 Mon> | https://www.reddit.com/r/programming/comments/64g2qp/programming_an_8bit_breadboard_computer/ | Programming 8 bit breadboard computer. Could be useful later.             |
| <2018-03-09 Fri> | https://news.ycombinator.com/item?id=14061985                                                 | Stuff about Erlang's BEAM vm - would be interesting to implement in CL    |
| <2017-11-16 Thu> | http://people.inf.ethz.ch/wirth/ProjectOberon/index.html                                                                           | Project Oberon book 2013 edition. Might be good.                                                  |

* Time/State

** Interesting response by Alan Kay
to a question on quora. I'll copy/paste it in here in case it's lost...
https://www.quora.com/Why-is-functional-programming-seen-as-the-opposite-of-OOP-rather-than-an-addition-to-it/answer/Alan-Kay-11

(via https://www.reddit.com/r/programming/comments/86hoz5/alan_kays_answer_to_why_is_functional_programming/)

It would be good to find out more about what was done in Interlisp - I don't know much about it. I wonder if I could make a metaclass for CLOS which would take care of making state changes atomic and logged. 

#+BEGIN_QUOTE
I hope for all our sakes that I can make this short …

In the latter part of the 50s John McCarthy got more and more interested in what he started to call “Artificial Intelligence”. He was also doing some consulting and this brought him in contact with the SAGE air defense system: large systems of very large computers attached to radar stations and each other and usable by graphical display systems with pointing devices.


John’s reaction was “Every home in America will have one of these”. He could see that the networked computers could be thought of as an “Information Utility” (as a parallel to the existing utilities for electricity, water, gas, etc…) and that the terminals in the homes could provide many kinds of “information services”. Among other things, this got him to advocate that MIT etc do “time-sharing” of their large mainframes …

He also realized that the computer milieu of the 50s — machine code and the new Fortran — did not intersect well with “most people in US homes”. This got him to write a paper in 1958 — “Programs With Common Sense” — and to suggest that what was needed for the user interface was an active semi-intelligent agent — the “Advice Taker” — that could interact with users in their commonsense terms, could learn from “taking advice”, could problem solve on behalf of the user and itself, and so forth (MIT AI Memo 17).

This got him thinking about how to implement such an Advice Taker, whose main mechanisms would be various kinds of logical deductions including those that required actions. There wasn’t much to go on back then but a few gestures at “list processing”, so he decided to invent a language that could be used to make the Advice Taker (and other kinds of robots), and more generally allow symbolic computation to take its place alongside the existing numerical computation.

John was an excellent mathematician and logician, and so he also wanted to come up with “A Mathematical Theory of Computation” to put ideas old and new on a firmer basis.

His result was LISP (for “LISt Processing”). I have written elsewhere about its significance.

Meanwhile, he was pondering just what kind of logic, math, and programming (he thought of these as highly intertwined) could be used to deal with a robot in the real world.

<eliminating detail here> A conflict was between at (robot, philadelphia) and at (robot, new york) which could not happen simultaneously, but could happen “over time”. This was like the problem of contemporary programming where variables would be overridden (and sometimes even files) — basically, letting the CPU of the computer determine “time”.

This destructive processing both allows race conditions and also makes reasoning difficult. John started thinking about modal logics, but then realized that simply keeping histories of changes and indexing them with a “pseudo-time” when a “fact” was asserted to hold, could allow functional and logical reasoning and processing. He termed “situations” all the “facts” that held at a particular time — a kind of a “layer” that cuts through the world lines of the histories. cf McCarthy “Situations, Actions, and Causal Laws” Stanford, 1963 prompted by Marvin Minsky for “Symbolic Information Processing”.

One of the ways of looking at this scheme is that “logical time” was simply to be included in the simulations, and that “CPU time” would not figure into any computation.

<more detail excluded here> This idea did not die, but it didn’t make it into the standard computing fads of that day, or even today. The dominant fad was to let the CPU run wild and try to protect with semaphores, etc. (These have the problem of system lockup, etc., but this weak style still is dominant.)

Systems that have used part or all of John’s insight include Strachey’s CPL, Lucid, Simula, etc. Look at Dave Jefferson’s TimeWarp schemes, Reed’s NetOS, Lamport’s Paxos, the Croquet system, etc.

To just pick just one of these, Strachey in the early 60s realized that tail recursion in Lisp was tantamount to “a loop with single simultaneous ‘functional assignment’ ”. And that writing it this way would be much clearer by bringing the computation of the *next* values for the variables together.

There are no race conditions possible because the right hand side of the assignments are all computed using old values of the variables, and the assignment itself is done to furnish new values for the variables all at once. (Looping and assignment can be clean if separate “time zones” are maintained, etc.)

More main stream is that big data systems used *versions* instead of overwriting, and “atomic transactions” to avoid race conditions.

Back to McCarthy and — now — objects. One of the things we realized at Parc was that it would be a very good idea to implement as much of John’s “situations” and “fluents” as possible, even if the histories were not kept very long.

For example, this would allow “real objects” to be world-lines of their stable states and they could get to their next stable state in a completely functional manner. In the Strachey sense, they would be “viewing themselves” with no race conditions to get their next version.

This would also be good for the multiple viewing we were starting to use. You really only want views to be allowed on stable objects (/relationships) and this can be done by restricting viewing to already computed “situational layers”.

Parc was also experimenting with “UNDO” and the larger community was starting to look at “parallel possible worlds reasoning”.

The acts of programming itself also wanted to be in terms of “histories and versions” and systems should be able to be rolled back to previous versions (including “values”, not just code). cf Interlisp, and especially the PIE system (done in Smalltalk by Goldstein and Bobrow).

This was another motivation for “deep John” in future systems. I.e. do everything in terms of world-lines and “simulated time”. A recent paper by Alex Warth shows some ways that “Worlds” can be quite fine-grained. http://www.vpri.org/pdf/tr201100...

The last point here is that “Histories R US”. I.e. we need *both* progression in time for most of our ideas and rememberings *and* we also want to reason clearly about how every detail was arrived at (and to advance the system).

John McCarthy showed us how to do this 60 years ago this year and wrote it down for everyone to read and understand.

So: both OOP and functional computation can be completely compatible (and should be!). There is no reason to munge state in objects, and there is no reason to invent “monads” in FP. We just have to realize that “computers are simulators” and figure out what to simulate.

I will be giving a talk on these ideas in July in Amsterdam (at the “CurryOn” conference).
#+END_QUOTE

Is this related to/similar to STM?

Here's something: https://github.com/cosmos72/stmx

Hmmm...

#+BEGIN_SRC lisp
  (defclass foo ()
    (x y)
    (:metaclass transactional))


  (setq x (make-instance 'foo))

  (setf (slot-value foo 'x) 1)
  ;; !!! ERROR: cannot set slot value outside transaction

  (with-transaction
      (trans)
    (setf (slot-value x 'x) 123)
    (format t "X = ~A~%" x))

  ;; !!! ERROR: slot x is unbound
  ;; - this happens because the change doesn't happen until transaction commit (outside the block)

  (with-transaction
      (trans)
    (setf (slot-value x 'x) 123
          (slot-value x 'y) 456))

  ;; there should be some way to ask for history too.
  ;; It's possible I could incorporate this into the postgres-class (or a subclass thereof). Is that helpful? I'm not sure.
  ;; It might be worth thinking about
#+END_SRC


* Other Programming Languages
** APL
- [[https://www.youtube.com/watch?v=9xCJ3BCIudI][Patterns and Anti-patterns in APL: Escaping the Beginner's Plateau]]

** Prolog
https://www.youtube.com/channel/UCFFeNyzCEQDS4KCecugmotg/videos - the Power of Prolog
https://www.cse.unt.edu/~tarau/research/2017/eng.pdf - A Hitchhiker’s
Guide to Reinventing a Prolog Machine

* General Notes about Computing
- https://news.ycombinator.com/item?id=16750733
- https://www.eejournal.com/article/fifty-or-sixty-years-of-processor-developmentfor-this/
- [[https://news.ycombinator.com/item?id=18037613][Question on HN about very good quality source]] which mentions NetBSD source for example.
* Functional Programming
- [[https://mostly-adequate.gitbooks.io/mostly-adequate-guide/][Professor Frisby's Mostly Adequate Guide to Functional Programming]]

* Parsing
- [[https://jeffreykegler.github.io/personal/timeline_v3][interesting article about parsing history]]


* EV3 Programming
I guess opening the BT serial device for the EV3 should be straightforward. Accessing it should look like this:-
#+BEGIN_SRC lisp
(with-open-file (stream "/dev/null"
                        :direction :io
                        :if-exists :append
                        :element-type '(unsigned-byte 8))
  (write-sequence #(1 2 3 4 5) stream)
  (let ((a (make-array 10 :element-type '(unsigned-byte 8))))
    (dotimes (i 5)
      (setf (aref a i) i))
    (read-sequence a stream :start 0 :end 4)
    a))
#+END_SRC

Then I just have to work out and implement the protocol. [[https://github.com/andiikaa/ev3ios][Here is an
implementation]] of the protocol in Swift. It defines all the constants
for the messages. [[https://le-www-live-s.legocdn.com/sc/media/files/ev3-developer-kit/lego%20mindstorms%20ev3%20communication%20developer%20kit-f691e7ad1e0c28a4cfb0835993d76ae3.pdf?la=en-gb][This is the communication protocol documentation
from Lego]]. It has some example messages which should be useful. It
should be easy enough to do. I can start by just trying some quick
examples with hand generated messages in CL. [[https://github.com/andiikaa/ev3ios/blob/master/EV3IOS/Ev3Enums.swift][This file]] documents the
constants used in the protocol. Seems easy enough... 

If I can make a good CL library to use this I could publish it. 

I have now gotten sending of commands (with no abstractions) to the EV3 from CL working:-

#+BEGIN_SRC lisp



(defparameter seq 0)

;; see: LEGO MINDSTORMS EV3 Communication Developer Kit

;; when this is opened it seems to be private to the thread which opens it.
;; That might mean I need to spin up a thread to handle the communication
(defparameter ev3 (open "/dev/tty.EV3-WirelessEV3"
                        :direction :io
                        :if-exists :append
                        :element-type '(unsigned-byte 8)))

;; (close ev3)

(defun ev3 (v)
  (let ((a (copy-seq v)))
    (setf (aref a 2) (logand #xff seq)
          (aref a 3) (ash (logand #xff00 seq) -8))
    (write-sequence a ev3)
    (incf seq)
    (finish-output ev3)))


(ev3 #(#x12 #x00
       #x01 #x00
       #x80 #x00 #x00 #xAE #x00 #x06 #x81 #x32 #x00 #x82 #x84 #x03 #x82 #xB4 #x00 #x01)
     )


;; (close ev3)

#+nil(ev3 #(#x2C #x00 #x00 #x00 #x80 #x00 #x00 #x84 #x13 #x00 #x82 #x00 #x00 #x82 #x00 #x00 #x84 #x1C #x01 #x82 #x00 #x00 #x82
           #x32 #x00 #x84 #x75 #x69 #x2F #x6D #x69 #x6E #x64 #x73 #x74 #x6F #x72 #x6D #x73 #x2E #x72 #x67 #x66 #x00 #x84 #x00))


;; so far so good - all working now that I'm flushing the output. Seems to be tied to just the 1 thread, but that's ok

#+END_SRC

* Interesting Programming UIs
[[https://origami.design/tutorials/getting-started/Getting-Started.html][Origami]] seems to be some kind of flowgraph interaction designer thing
done by people at FB. Looks a bit like Quartz Composer but more for
interaction design rather than visualizations.

* Type Checking
http://okmij.org/ftp/ML/generalization.html found via https://news.ycombinator.com/item?id=18458569
* Bash Scripting Cheat Sheet
https://devhints.io/bash

* Misc
[[http://www.objective.st/][Objective Smalltalk]] looks interesting (via [[https://news.ycombinator.com/item?id=18964665][Hacker News]]). A smalltalk
on the obj-c runtime. A bit like F-script I guess, but I think it's
designed to be more programming-languagey (ie full).

There is more information about it here:-
- https://news.ycombinator.com/item?id=21552264
- https://blog.metaobject.com/2019/11/presenting-in-objective-smalltalk.html
- https://github.com/mpw/Objective-Smalltalk

- [[http://degoes.net/articles/fp-glossary][Functional Programming Glossary]]

- [[https://leanpub.com/progalgs/read][Programming Algorithms]] book using CL

* Regular Expression Speed
https://swtch.com/~rsc/regexp/regexp1.html - interesting about regex
implementations. The following blows up in time in perl:
~"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"=~/a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/~
but runs in microseconds with CL-PPCRE ~(scan "a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?a?aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")~
which suggests that Edi Weitz's implementation must be using the theoretically good approach (NFA/DFA?).

https://github.com/google/re2/wiki/WhyRE2

Found from this discussion:
https://news.ycombinator.com/item?id=20336332 as CloudFlare had
downtime because of a regex causing 100% CPU usage in their Web
Application Firewall managed rules.
* Differentiable Programming
https://github.com/apple/swift/blob/master/docs/DifferentiableProgramming.md via [[https://news.ycombinator.com/item?id=21521233][Hacker News]]
https://2019.ecoop.org/details/ecoop-2019-papers/11/Automatic-Differentiation-for-Dummies with Simon Peyton Jones (https://news.ycombinator.com/item?id=22343285)
* Make
I should probably make more use of Makefiles. They're handy. Along
with git make is probably a very good basis tool to have. I wonder if
I could even use make for installing dependencies. For example, if I'm
building a lisp project it could have something for installing CCL if
not present (though maybe with a special 'environment' target or
something). Once (say) quicklisp is bootstrapped via make we can use
ql to do the rest. 

[[https://blog.mindlessness.life/makefile/2019/11/17/the-language-agnostic-all-purpose-incredible-makefile.html][The Language Agnostic, All-Purpose, Incredible, Makefile]] via [[https://news.ycombinator.com/item?id=21566530][Hacker
News]]

It might also be a good way of updating the database. Either by
incrementally creating required tables and making files to mark what
has been done and/or by syncing the database from the server. Probably
make will be useful to do the things that git push doesn't cover. 


* Glamorous toolkit looks interesting
https://gtoolkit.com
interactive dev environment derived from smalltalk
