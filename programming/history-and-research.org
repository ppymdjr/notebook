#+STARTUP: indent
#+TITLE: Programming Language and Computing History and Research

* History
I think it might be worth trying to trace the history of various
projects and people so as to find connections with things. I could put
things into a database. That might make it easier to find interesting
things. I'm very curious about histories and timelines of things. I
wonder if anyone else has done anything like that. Probably in some
domains. 
** Dylan
Apple Dylan was an interesting thing developed at Apple Cambridge and
then axed. It was developed by a bunch of MCL(?) lisp
programmers. Does it seem that MCL never really adopted CLIM and that
whole approach? There doesn't seem to be much evidence of it in
Dylan's UI stuff or in MCL (even though that seemed good). MCL seemed
to have a nice, but fairly traditional GUI builder which just wrapped
the Mac toolbox. CLIM is quite different from that.

http://opendylan.org/about/apple-dylan/

* MacFrames, SK8, Ralph etc

So, there was a KEE type system called MacFrames apparently. It was
used to automate testing of System 7. There was a lisp based LM like
OS devised for the Newton (before the final OS for that was made)
which did CLIM like things like I'm doing. 

My reading all started here:-
- https://news.ycombinator.com/item?id=12703498
- http://lispm.de/lisp-based-newton-os (via comments in above)

It might be worth looking at what was done with Interlisp D too:-
http://www.textfiles.com/bitsavers/pdf/xerox/interlisp/3101273_InterlispD_2_Oct85.pdf
http://www.ics.uci.edu/~andre/ics228s2006/teitelmanmasinter.pdf - the
Interlisp programming environment

http://homepages.cwi.nl/~steven/sigchi/bulletin/1998.2/spohrer.html

Something called SK8, which I now have source and executables for, was
developed in MCL and used the MacFrames object model it seems. A frame
based object model is probably quite like what I want/need for
modelling all the fleet stuff.

http://monishrathod.blogspot.de/2010/01/skating.html

It might be worth trying to get a Mac emulator up (SheepShaver?) to
run SK8 and MCL (for building) to have a look at this stuff. 

Interestingly there must still be people at Apple working on some of
these things, since OSA is supported from JS. 

So, the following is really interesting, because we have JS code which
is running stuff which just returns these kind of references. It's
very much like the SQL query sequence stuff in this regard.

#+BEGIN_SRC javascript
>> s = Application('System Events')
=> Application("System Events")
>> s
=> Application("System Events")
>> s.processes
=> Application("System Events").processes
>> s.processes['iTunes']
=> Application("System Events").processes.byName("iTunes")
#+END_SRC

This means that the whole OSA object model is very different from a
'normal' object model and a lot more like the one that I want to
implement, which will fit very naturally with the database.

It has all these interesting symbolic references. 

It's also worth comparing with the SQL interface to general OS stuff
provided by that thing I found recently.

SK8:-
set the fillColor of every rectangle whose fillColor = blue and ¬
   whose top < 250 in mainWindow to any item in the ¬
   knownChildren of RGBColor

A little bit more [[https://www.reddit.com/r/lisp/comments/4oo1cp/common_lisp_for_clojure_programmer/d4eec68/][here]] about MacFrames in case I haven't got it. 

I have [[file:/Users/david/Library/Mobile Documents/com~apple~CloudDocs/Development/archive/apple_sk8][downloaded SK8 sources]] onto my computer so I can potentially
build it if I set up a dev environment. Could be interesting anyway. I
thought I already had them somewhere.

Also, [[https://news.ycombinator.com/threads?id=mikelevins][here]] is an HN link showing all of mikelevings comments.

* Lisp Machine etc
I wonder if I can put all the downloaded papers in this GIT
repo. Depends on copyright really. Otherwise I'll try and link to the
source.

| (downloaded paper) [5]                                                   | CLIM User Guide                                                                                                        |
| (downloaded paper)                                                       | Lisp Lore: A Guide to Programming the Lisp Machine                                                                     |
| (downloaded paper)                                                       | Visibly Controllable Computing: The Relevance of Dynamic Object Oriented Architectures and Plan Based Computing Models |
| (downloaded paper)                                                       | Presentation Based User Interfaces                                                                                     |
| (followed tutorial to install and experiment with Lisp Machine emulator) | Set up a Lisp Machine emulator running Genera                                                                          |
| (downloaded manual)                                                      | Lisp Machine Window System Manual                                                                                      |
| (downloaded manual)                                                      | Genera Workbook                                                                                                        |
| http://lispm.de/lisp-based-newton-os                                     | Lisp Based Newton OS                                                                                                   |
| (book - Very Useful)                                                     | The Art of the Metaobject Protocol                                                                                     |
| (downloaded manual)                                                      | Common Lisp Interface Manager (CLIM) Release 2.0                                                                       |
| https://news.ycombinator.com/item?id=12703498                            | KEE, MacFrames &c research                                                                                             |
| http://monishrathod.blogspot.fr/2010/01/skating.html                     | SK8                                                                                                                    |
| http://www.cs.virginia.edu/~evans/cs655/readings/smalltalk.html          | Design Principles Behind Smalltalk                                                                                     |

I'm making some progress with getting this set up. I have an Ubuntu VM
ready to run it. Not sure why it's not running though...

I think it failed while running the postinstall script for some
reason. I think it was chef and/or puppet which failed.

See ~/Downloads/opengenera/saved-make.log::Checking user - OK (this link broke my build)

** I think this here was the problem:-

checking whether build environment is sane... configure: error: newly created file is older than distributed files!
Check your system clock
ERROR: exit code 1
Error executing command echo 'vagrant'|sudo -S sh './postinstall.sh' : Exitcode was not what we expected
Exitcode was not what we expected
Checking user - OK

Now that I'm running the postinstall.sh again it seems to be getting
further though. Let's see...

opengenera/provision.sh looks fairly straightforward. I think I have
everything installed that I need to download. I think I can just
manually execute the commands in that script and then get everything
up and running. I think. Worth a try if I feel like it...

I might need to set up a shared folder for the VM OR just copy in all
the files that it needs. I can always redo all of this if it doesn't
work out (and blow the machine away). I could also build it on
legion, which would be quicker due to internet speed.

Victory!!! It's alive. Boots anyway. Basically this:-

#+BEGIN_SRC lisp
./genera -geometry 1150x900
#+END_SRC

From the ratpoison vnc session. Now what? Configure site? Login?
Probably...

#+BEGIN_SRC sh :output :none
open vnc://localhost:5901
#+END_SRC

#+RESULTS:

It seems that ASDF does support genera, so it should be possible to
get that installed. I don't know how many of the ASDF packages support
genera, but I imagine pure CL ones should stand a good chance. 

I think that, sooner or later, I am going to need to build a full IDE
for the web so that I can do CL dev from a web browser. I'm not
exactly sure how that will work. Ideally I need to get the debugger
working properly with restarts. That probably basically means that I
need to write a debugger. It can't be that hard.

Said debugger would need to take account of user access - not everyone
should have full access to the debugger.

It might also need a WS connection to handle that straightforwardly.

This is a bit interesting, and I have the source now:-
http://www.ai.sri.com/~delacaze/alu-site/alu/luv/96/1.SUB8-3.html

(probably not super useful though - probably best to look at CLIM in
general) 

Also:-
http://clhttp.plasticki.com/show?4EX

... I have the source to that too. Probably not that useful. w3p is
included as part of that.


** Interesting information
- LispM museum article
  - https://news.ycombinator.com/item?id=12703498

It turns out that before NewtonOS there was a team at Apple working on
(essentially) a lisp machine like OS for a newton like device:-
http://lispm.de/lisp-based-newton-os - this is defunct. It can be
found on [[https://web.archive.org/web/20140621025142/http://lispm.de/lisp-based-newton-os][WayBack Machine]]. Maybe I should mirror it? That link also
cropped up on [[https://news.ycombinator.com/item?id=7781265][Hacker News]] where there is a pastebin link of it.

It used MCL to cross compile a scheme-with-CLOS like language to
ARM. Lots of intersting concepts (rather CLIM like). Very interesting
ideas.

Also used a frame based system for reasoning about what to
display. That sounds interesting. 

#+BEGIN_QUOTE
Q: Was there a User Interface designer?

Mikel: No. The UI implementor instead spent his time writing a
declarative specification language for UI, which was intended to be
easy enough to use that you wouldn't need a UI designer. You would
control what elements appeared using type annotations, and would
control how they looked using the stylesheets. The idea sort of
foreshadowed DHTML and CSS and XUL.
#+END_QUOTE


** Docker
Someone has made a docker container with VLM in it:-
https://github.com/sethm/docker-vlm

** More guides
https://www.reddit.com/r/lisp/comments/4nrgz7/running_open_genera_20_on_linux_the_easy_way/
https://static.loomcom.com/genera/genera-install.html
https://archives.loomcom.com/genera/genera-install.html
** Other
http://www.nhplace.com/kent/Papers/cl-untold-story.html - from Kent Pitman
** MacLisp
This is a website about MacLisp: http://www.maclisp.info, including
the [[http://www.maclisp.info/pitmanual/index.html][Pitmanual]] for it. 
** notes
It would be good if I could make a pushable genera VM to run on
SmartOS. 
* Random Links
[[https://netzhansa.blogspot.com/2018/08/evaluating-vax-lisp-30.html][Evaluating VAX Lisp 3.1]] [[https://news.ycombinator.com/item?id=17821951][via HN]], which is based on CLtL 1, so it's not
quite CL, but heading towards it. [[https://github.com/simh/simh][Here is a VAX emulator]] which I guess
could be used to run that.

It would be interesting to set up a 'museum' of old system emulators. It would be interesting, expensive, time consuming and difficult to set up a museum of old hardware. 

[[https://news.ycombinator.com/item?id=13954077][James Mickens article]]

[[https://twobithistory.org/2019/03/31/bbc-micro.html][Codecademy vs BBC]] is an interesting articla (via [[https://news.ycombinator.com/item?id=19603106][Hacker News]]) about
the 80s history of computing education. There's a cool historical
video archive of the BBC's computer literacy project [[https://computer-literacy-project.pilots.bbcconnectedstudio.co.uk/][here]].

https://github.com/spec-chum/Amiga-Scoopex-C - Amiga hardware programming tutorials

[[https://news.ycombinator.com/item?id=25176318][Hacker News]] link about the birth of unix with Brian Kernighan

[[https://www.youtube.com/watch?v=-CWO3IpCco8&list=PLvL2NEhYV4Zu0Jrp0l90aU83_AfuFcN_q&fbclid=IwAR2XxjlWB3hHOK1eEpOf7yuLcyuEscfjKTXAaQLZBPzfX62P0VXkaj4ESic][Cynthia Solomon talk]] about Logo and teaching children. Quite interesting.

* Emulation
Useful for historic research etc (playing with old CL systems, SK8,
whatever...).

- https://www.jamesbadger.ca/2018/11/07/emulate-mac-os-9-with-qemu/ via [[https://news.ycombinator.com/item?id=18585157][Hacker News]].

Apparently QEmu's latest version can now run MacOS 9.2.2 with sound, networking and good speed. Would be good to set up to test MCL &c.

https://www.google.com/search?q=qemu+Mac+OS+9.2.2&oq=qemu+Mac+OS+9.2.2
https://www.reddit.com/r/emulation/comments/9mx3b7/man_mac_os_922_is_running_great_in_qemu/
[[https://news.ycombinator.com/item?id=18959067][Advanced Mac Substitute]] looks interesting too.

http://vpri.org/talks.htm

Via [[https://news.ycombinator.com/item?id=20187762][Hacker News]] and [[https://blog.nootch.net/post/amiga-bbs-online-2019/][interesting article about Amiga BBS]] in 2019
describing use of MiST - FPGA system which can run emulations of
various systems. It has been kind of superseded by [[https://github.com/MiSTer-devel/Main_MiSTer/wiki][MiSTer]] which is a
more powerful implementation. 
** Mac Classic
Found a bunch of links from HN. Someone got a swift app compiled for
Mac Classic. There are various links about compiling apps for classic
etc. I would like to get a classic mac emulator running. Don't really
know why though. Might just be cool to have a really suped up
implementation of something I used to use. I would also like to try
and resurrect the G4 iBook. I would have to resolder the power switch
cables to the mobo I think. That machine could run class MacOS,
NetBSD, OpenBSD, OSX or whatever. It's battery might not work. 
- https://news.ycombinator.com/item?id=22754667 
- https://www.highcaffeinecontent.com/blog/20150124-MPW,-Carbon-and-building-Classic-Mac-OS-apps-in-OS-X
- https://github.com/autc04/Retro68
- https://github.com/zydeco/macemu/tree/ios/BasiliskII/src/iOS
- https://www.reddit.com/r/retrobattlestations/comments/n1xa7s/macintosh_system_7_on_ipad_pro/

* Amiga Machine code programming
https://www.markwrobel.dk/project/amigamachinecode/
* Retro computing
- https://github.com/a2stuff/vnIIc - 'VNC' for apple 2c. I should be able to run this - I have the hardware. 

https://a2stuff.github.io/vnIIc/ (linked from the above) seems to work
entirely from the browser, which is pretty amazing - it's asking for
serial port access. I'm quite tempted to try this. Just because. 

By streaming a Chrome tab instead of the whole screen, and setting its
size to be the same as the 2c screen resolution (or, if pixels are not
square then a multiple) and, say, putting a canvas in it it will give
me the ability to just effectively draw straight on the 2c screen. The
same could be done with a different application window.

I tried it and it worked once, but then not again. Not sure why. I'll
try it again. Seems to work well though when it did.

It has keyboard and mouse input too, so the 2c can interact with
whatever it is. I guess that would work well with a custom web
page. Although I'm not sure the streamed Chrome tab will get key
events - I think they'll just be sent to the driving web page. That
could probably work though. I'll hvae to play with it.

Also of note: the 2c serial port can run quite fast (115,200 kbps)
which should make a very usable serial terminal, albeit small (80x24
or so). It's probably not *really* useful I guess, but kind of
cool. Of course, the RPi B can drive the monochrome monitor just
fine. Depends on whether there's any use to having a console. They
keyboard is quite usable now without the rubber mat.

Maybe there's one here: https://mirrors.apple2.org.za/ftp.apple.asimov.net/images/communications/

Internal drive of my 2c doesn't seem to be working. The following (from https://apple2.fandom.com/wiki/Booting_from_an_external_drive_on_the_Apple_IIc) will boot from the external drive:-
#+begin_src basic
call -151
300:A9 E0 A0 01 A2 60 4C 0B C6

300G
#+end_src 

(The heads probably just need a clean but there are various guides to fixing these issues. It worked recently)

There's something called Apple 2 cloud which looks pretty useful to do stuff with the 2c: http://appleii.ivanx.com/prnumber6/category/a2cloud/ 

From the above people seem to recommend something called ProTerm. It
looks rather good. Version 3.1 (the latest) was made freeware in 2009!

There are also designs for an [[http://www.retroadventures.net/apple-iic-and-the-d-i-wimodem232/][ESP8266 based WiFi]] 'modem' which works
with a 2c and allows it to directly connect to the WiFi and then
telnet into BBS systems! Presumably I could have it telnet into a
machine here on the network. Is that any better than just using a
serial connection to another machine (like I did on my laptop)?

http://subethasoftware.com/2018/02/28/wire-up-your-own-rs-232-wifi-modem-for-under-10-using-esp8266-and-zimodem-firmware/

** Modem Manager
I have this backed up on my machine now. The incantations to fire up a
9600 baud vt220 terminal are: ~Esc M N C-Esc : V~ (not sure if case
sensitive).

I found some information here:
http://www.club.cc.cmu.edu/~mdille3/doc/mac_osx_serial_console.html
about this.

~/Library/LaunchDaemons/serialconsole.plist~

#+begin_src xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
        <key>Label</key>
        <string>serialconsole</string>
        <key>ProgramArguments</key>
        <array>
                <string>/usr/libexec/getty</string>
                <string>std.9600</string>
                <string>cu.usbserial-1410</string>
        </array>
        <key>KeepAlive</key>
        <true/>
</dict>
</plist>
#+end_src

~sudo launchctl load /Library/LaunchDaemons/serialconsole.plist~

For the serial interface I have the above gives a login console on the 2c. This probably isn't *really* useful. 

** ROM Version
~print peek(64447)~ prints the ROM version. In my case '0'. That was
introduced in November 1985. The next one came out in September 1986
making it likely the 2c I have was purchased in 1986.
** IBM 1401 info
https://www.curiousmarc.com/computing/ibm-1401-mainframe/ibm-1401-programming
* Networking
The OSI model is an abstract model of an entirely /different/ computer
networking system than what the internet uses. The OSI model doesn't
describe the internet at all. More information [[https://computer.rip/2021-03-27-the-actual-osi-model.html][here]] via [[https://news.ycombinator.com/item?id=26607983][HN]]. 

So in this ISO networking model there was an entirely different email
system called [[https://en.wikipedia.org/wiki/X.400][X.400]] which, until recently (2007 I think) was
implemented in Exchange! It was very complicated. It allowed messages
to be addressed with various different fields and was expected to be
able to route email even with just some of the information. For
example, you could address an email to 'S=Ritchie;G=David;C=GB' and it
/might/ get to me if email delivery was generally handled by a central
government agency (like the GPO I guess) - which is what X.400 rather
expected. 

Most of that ISO OSI model isn't used these days. One part which /is/
used, though, is x509 - which is why it's kind of complicated. 

X.500 is directory services, which was a more complicated version of
LDAP, itself very complicated. Obviously people imagined things would
be managed in a more centralized sort of way than has been done with
the internet. 

Speaking of old network protocols, I was interested to find that
Classilla (browser for 'Classic' Mac OS, which is sadly being
discontinued) is published via the [[gopher://gopher.floodgap.com/1/gopher/clients/mac/classilla/][Floodgap Gopher server]]. This makes
sense, since it's rather difficult to download a browser without
having a browser and a Gopher client is /much/ easier to
implement. Which leads to a though: the modern web can only be browsed
on up to date systems due to the /huge/ complexity of all the stuff
involved. This effectively makes Gopher space's use case somewhat like
Latin for the Catholic church - it's essentially a dead protocol so it
works well across time. 

Maybe I should publish these notes in Gopherspace... 

I guess the other thing about Gopherspace is that it hasn't been
'hijacked' by money making corporations - not even slightly! It's kind
of like the internet from the olden days.  

Of course, org would work well for making gopher files I suppose. I
wonder if emacs would honor modelines in them. Doesn't really matter
though.

I should make a SmartOS zone to run the gopher server, forward port
70, make a dataset to store the site etc. Will I need to give it a
fixed IP if I'm going to forward that way? Maybe I can coax Treafik
into forwarding port 70 - yup, should work. Just don't enable TLS. 

Maybe [[https://github.com/gophernicus/gophernicus][gophernicus]] is better to use than gofish. It auto generates maps
so I can just drop in files. 
